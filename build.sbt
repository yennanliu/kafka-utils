scalaVersion := "2.13.8"

libraryDependencies ++= Seq(
  "org.typelevel" %% "mouse" % "1.1.0",
  "org.fusesource.jansi" % "jansi" % "2.4.0",
  "ch.qos.logback" % "logback-classic" % "1.2.11",
  "org.typelevel" %% "log4cats-slf4j" % "2.4.0",
  "com.github.pureconfig" %% "pureconfig" % "0.17.1",
  "com.github.pureconfig" %% "pureconfig-cats-effect" % "0.17.1",
  "com.github.fd4s" %% "fs2-kafka" % "2.5.0",
  "com.github.scopt" %% "scopt" % "4.1.0",
)

val tylipPublic =
  "tylip-public" at "https://tylip.jfrog.io/artifactory/tylip-public/"

resolvers += tylipPublic

scalafmtOnCompile := !insideCI.value

scalacOptions ++= Seq(
  "-deprecation",
)
