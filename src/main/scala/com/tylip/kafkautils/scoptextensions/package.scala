package com.tylip.kafkautils

import java.time.OffsetDateTime

import scopt.Read

package object scoptextensions {

  implicit val offsetDateTimeRead: Read[OffsetDateTime] =
    Read.stringRead.map(OffsetDateTime.parse)

}
