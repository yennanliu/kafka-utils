package com.tylip.kafkautils

import java.time.OffsetDateTime

import cats.effect.{ExitCode, IO, IOApp}
import cats.instances.option._
import cats.syntax.apply._
import cats.syntax.option._
import scoptextensions._

final case class KafkaUtilsOpts(
  sourceTopic: Option[String] = None,
  targetTopic: Option[String] = None,
  splitMoment: Option[OffsetDateTime] = None,
) {
  def copyTopicParams: Option[CopyTopicParams] =
    (sourceTopic, targetTopic)
      .mapN(CopyTopicParams.apply(_, _, splitMoment.map(_.toInstant)))
}

object KafkaUtilsIOApp extends IOApp {

  private val parser = new scopt.OptionParser[KafkaUtilsOpts]("scopt") {
    head("kafka-utils")
    opt[String]('s', "sourceTopic")
      .action((x, c) => c.copy(sourceTopic = x.some))
    opt[String]('t', "targetTopic")
      .action((x, c) => c.copy(targetTopic = x.some))
    opt[OffsetDateTime]('m', "splitMoment")
      .action((x, c) => c.copy(splitMoment = x.some))
  }

  def run(args: List[String]): IO[ExitCode] =
    parser
      .parse(args, KafkaUtilsOpts()).flatMap(_.copyTopicParams).fold(
        IO(println("Wrong arguments!")).as(ExitCode.Error),
      )(
        KafkaUtils
          .copyUntilResource[IO](_)
          .use(_.compile.drain).as(ExitCode.Success),
      )

}
