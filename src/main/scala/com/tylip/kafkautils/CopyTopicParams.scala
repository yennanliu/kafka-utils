package com.tylip.kafkautils

import java.time.Instant

final case class CopyTopicParams(
  sourceTopic: String,
  targetTopic: String,
  splitMoment: Option[Instant],
)
